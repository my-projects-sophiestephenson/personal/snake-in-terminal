import java.util.*;

public class Board {

    // Attributes
    private String[][] board;
    private ArrayList<int[]> snake;
    private int[] food;
    private int moves;
    private int foodPoints;
    private boolean standbyLengthen; // Is true if the snake ate a piece of food on a 5th turn, thus needing
    // to be lengthened in two subsequent turns
    private boolean up, down, left, right;

    // Constants
    private static final int BOARDSIZE = 11, STARTER_SNAKE_SIZE = 5;
    private static final char UP = 'w', DOWN = 's', LEFT = 'a', RIGHT = 'd';
    private static final String HEAD_UP = "^", HEAD_DOWN = "v", HEAD_LEFT = "<",
            HEAD_RIGHT = ">", BODY = "0", EMPTY = "_", FOOD = "X";

    // Constructor
    public Board() {
        // Create a new, empty board of the right size
        board = new String[BOARDSIZE][BOARDSIZE];

        up = true;

        // Set starter coordinates for snake
        snake = new ArrayList<>(STARTER_SNAKE_SIZE);
        for (int i = STARTER_SNAKE_SIZE - 1; i >= 0; i--) {
            snake.add(new int[] {BOARDSIZE - i - 1, BOARDSIZE / 2});
        }

        // Finally, add one X to the board in a random place (not occupied by the snake)
        do {
            int row = (int) (Math.random() * ((BOARDSIZE - 1) + 1));
            int col = (int) (Math.random() * ((BOARDSIZE - 1) + 1));
            food = new int[] {row, col};
        } while (snakeCoversPos(food));

        // Add snake and food coordinates to board
        updateBoard();
    }

    // Getters & Setters
    public String[][] getBoard() {
        return board;
    }

    public ArrayList<int[]> getSnake() {
        return snake;
    }
    public void lengthenSnake() {
        int size = snake.size();

        snake.add(new int[] {snake.get(size - 2)[0], snake.get(size - 2)[1]});
    }

    public int[] getFood() {
        return food;
    }
    public void newFood() {
        // Reset old food pos
        board[food[0]][food[1]] = EMPTY;
        // Set new food pos, making sure it's not a place where the snake is
        do {
            int row = (int) (Math.random() * ((BOARDSIZE - 1) + 1));
            int col = (int) (Math.random() * ((BOARDSIZE - 1) + 1));
            food = new int[] {row, col};
        } while (snakeCoversPos(food));
        board[food[0]][food[1]] = FOOD;
        incFoodPoints();
    }

    public int getMoves() {
        return moves;
    }
    public void incMoves() {
        moves++;
    }

    public int getFoodPoints() {
        return foodPoints;
    }
    public void incFoodPoints() {
        foodPoints++;
    }


    // Methods
    public void printBoard() {
        for (int row = 0; row < BOARDSIZE; row++) {
            for (int col = 0; col < BOARDSIZE; col++) {
                System.out.print(board[row][col]);
                if (board[row][col].length() == 1)
                    System.out.print("  ");
                else
                    System.out.print(" ");
            }
            if (row == 0) {
                System.out.print("      Moves: " + moves);
            }
            if (row == 0) {
                System.out.print("      Food Eaten: " + foodPoints);
            }
            System.out.println();
            System.out.println();
        }
        System.out.println();
    }

    private void updateBoard() {
        // Set empty board
        for (int row = 0; row < BOARDSIZE; row++) {
            for (int col = 0; col < BOARDSIZE; col++) {
                board[row][col] = EMPTY;
            }
        }

        // Add food coordinates to board
        board[food[0]][food[1]] = FOOD;

        // Add snake coordinates to board
        if (up)
            board[snake.get(0)[0]][snake.get(0)[1]] = HEAD_UP;
        else if (down)
            board[snake.get(0)[0]][snake.get(0)[1]] = HEAD_DOWN;
        else if (left)
            board[snake.get(0)[0]][snake.get(0)[1]] = HEAD_LEFT;
        else
            board[snake.get(0)[0]][snake.get(0)[1]] = HEAD_RIGHT;

        for (int i = 1; i < snake.size(); i++) {
            board[snake.get(i)[0]][snake.get(i)[1]] = BODY;   //Integer.toString(i + 1);
        }
    }

    private boolean posAreEqual(int[] pos1, int[] pos2) {
        return pos1[0] == pos2[0] && pos1[1] == pos2[1];
    }

    private boolean snakeCoversPos(int[] pos) {
        for (int[] coord : snake) {
            if (posAreEqual(coord, pos))
                return true;
        }
        return false;
    }

    private boolean moveIsOutOfBounds(char direction) {
        if (direction == UP)
            return snake.get(0)[0] - 1 < 0;
        else if (direction == DOWN)
            return snake.get(0)[0] + 1 >=  BOARDSIZE;
        else if (direction == LEFT)
            return snake.get(0)[1] - 1 < 0;
        else if (direction == RIGHT)
            return snake.get(0)[1] + 1 >= BOARDSIZE;
        else
            return true;
    }

    // private boolean moveIsOutOfBounds(KeyEvent event) {

    // }

    private int[] getNextFrontPos(char direction) {
        int[] result;

        if (direction == UP)
            result = new int[] {snake.get(0)[0] - 1, snake.get(0)[1]};
        else if (direction == DOWN)
            result = new int[] {snake.get(0)[0] + 1, snake.get(0)[1]};
        else if (direction == LEFT)
            result = new int[] {snake.get(0)[0], snake.get(0)[1] - 1};
        else if (direction == RIGHT)
            result = new int[] {snake.get(0)[0], snake.get(0)[1] + 1};
        else
            result = new int[] {-1, -1};

        return result;
    }

    private boolean pathIsClear(char direction) {
        // Store the next front position of the snake
        int[] nextFrontPos = getNextFrontPos(direction);
        int[] lastSnakePos = new int[] {snake.get(snake.size() - 1)[0],
                snake.get(snake.size() - 1)[1]};

        // Makes sure that it won't hit the snake
        // This is true if 1. the snake isn't in the way at all
        //                 2. the tail of the snake is in the way but it's not going to lengthen in the next turn
        return !snakeCoversPos(nextFrontPos) ||
                (posAreEqual(nextFrontPos, lastSnakePos) &&
                        !posAreEqual(nextFrontPos, food) &&
                        (moves + 2) % 5 != 0);
    }

    private boolean moveEatsFood(char direction) {
        // Store the next front position of the snake
        int[] nextFrontPos = getNextFrontPos(direction);

        return posAreEqual(nextFrontPos, food);
    }

    public boolean isMoveValid(char direction) {
        return (pathIsClear(direction) && !moveIsOutOfBounds(direction));
    }

    public void moveSnake(char direction) {

        switch (direction) {
            case UP : 	{
                up = true;
                down = false;
                left = false;
                right = false;
                break; }
            case DOWN : {
                up = false;
                down = true;
                left = false;
                right = false;
                break; }
            case LEFT : {
                up = false;
                down = false;
                left = true;
                right = false;
                break; }
            case RIGHT : {
                up = false;
                down = false;
                left = false;
                right = true;
                break; }
        }

        // If it eats food AND it's the 5th turn, lengthen the snake and
        //    add a standby lengthen so it will lengthen the snake on the
        //    next turn
        if ((moves + 1) % 5 == 0 && moveEatsFood(direction)) {
            lengthenSnake();
            newFood();
            standbyLengthen = true;


        } // If the move eats food or it's the 5th turn, lengthen the snake
        else if ((moves + 1) % 5 == 0)
            lengthenSnake();

            // If the move eats food, lengthen the snake and add a new food
        else if (moveEatsFood(direction)) {
            lengthenSnake();
            newFood();
        }

        // If there is a standby lengthen (and it's not already being lengthened
        //    this move), lengthen the snake
        else if (standbyLengthen) {
            lengthenSnake();
            standbyLengthen = false;
        }

        // Move each coordinate to where the one before it was
        for (int i = snake.size() - 1; i > 0; i--) {
            snake.get(i)[0] = snake.get(i - 1)[0];
            snake.get(i)[1] = snake.get(i - 1)[1];
        }

        // Update the first coordinate so it goes where the user wants
        snake.set(0, getNextFrontPos(direction));
        updateBoard();
        incMoves();
    }


}
