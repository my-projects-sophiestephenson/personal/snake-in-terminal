## Snake (played in terminal)

### Overview
Allows a user to play Snake in the terminal. The player presses w, s, a, and d 
to move the snake up, down, left, and right, respectively, and the game is over 
when the snake runs into itself or hits the edge of the board. The snake gets
longer every 5 moves or when the snake eats a piece of food, represented by an X
on the board.

### How to Use

To play, import all files and run the following two lines in the terminal:
```sd
javac *.java
java Snake
```