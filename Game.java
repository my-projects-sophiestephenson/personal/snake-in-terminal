public class Game {

    // Attributes
    private Board board;
    private boolean inProgress;

    // Constants
    private static final String PLAY_MSG = "Enter a command (w, s, a, d) or q to quit:";
    private static final String INVALID_COMMAND_MSG = "Invalid command, please try again.";
    private static final String GAME_OVER_MSG = "Game over!";
    private static final String WELCOME_MSG =   "\n           WELCOME TO SNAKE\n" +
            "---------------------------------------\n" +
            "   Goal: Make your snake as long as\n" +
            "     possible without running into\n" +
            "   the snake or the edge of the board.\n\n" +
            "   Controls:       W\n" +
            "                A  S  D\n\n" +
            "  Press W to to up, S to go down, A to\n" +
            "       go left, and D to go right.\n\n" +
            "  Ready to play? Type \"y\" to start or \n" +
            "             \"q\" to quit!";
    private static final char UP = 'w', DOWN = 's', LEFT = 'a', RIGHT = 'd', QUIT = 'q', YES = 'y';

    // Constructors
    public Game() {
        board = new Board();
    }

    // Getters & Setters
    public Board getBoard() {
        return board;
    }

    public void startGame() {
        inProgress = true;
    }
    public void endGame() {
        inProgress = false;
    }

    // Methods
    public void play() {

        EasyIn2 reader = new EasyIn2();
        System.out.println(WELCOME_MSG);
        char s = reader.getChar();
        while (!(s == YES || s == QUIT)) {
            System.out.println("Try again.");
            s = reader.getChar();
        }
        if (s == QUIT) {
            endGame();
            System.out.println("Goodbye!");
        }
        else
            startGame();

        while (inProgress) {

            board.printBoard();

            System.out.println(PLAY_MSG);

            char direction = reader.getChar();

            if (direction == QUIT)
                endGame();
            else if (!directionIsValid(direction))
                System.out.println(INVALID_COMMAND_MSG);
            else if (!board.isMoveValid(direction)) {
                endGame();
                System.out.println(GAME_OVER_MSG);
                printScore();
            } else
                board.moveSnake(direction);
        }
    }

    public boolean directionIsValid(char direction) {
        return (direction == UP ||
                direction == DOWN ||
                direction == LEFT ||
                direction == RIGHT);
    }

    public void printScore() {
        System.out.println("Total Moves: " + board.getMoves() + ", Total Food Eaten: " + board.getFoodPoints());
    }


}